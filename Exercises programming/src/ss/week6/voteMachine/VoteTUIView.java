package ss.week6.voteMachine;
import java.util.Scanner;
import java.util.Map;
import java.util.Observable;
import java.util.Set;
import java.util.List;
import java.util.Observer;

public class VoteTUIView implements Observer, VoteView {
	
	VoteMachine votemachine = new VoteMachine();

	public VoteTUIView(VoteMachine votemachine) {
		this.votemachine = votemachine;
	}

	public void start(){
		boolean run = true;
		Scanner scanner = new Scanner(System.in);
		String words = null;
		String[] wordsarray = null;
		
		while (run){
			System.out.println("What do you want to do? VOTE, ADD PARTY, VOTES, PARTIES, EXIT, HELP");
	

			if (scanner.hasNextLine()){
				words = scanner.nextLine();
				wordsarray = words.split(" ");
				
			}
				if(wordsarray[0].equals("VOTE")){
					if(wordsarray.length == 2){
						String party = wordsarray[1];
						votemachine.vote(party);
						System.out.println("You voted for " + party);
						run = true;
					} else {
						System.out.println("This command was unvalid");
						run = true;
					}
				}
				
				else if(wordsarray[0].equals("ADD") && wordsarray[1].equals("PARTY")){
					if (wordsarray.length == 3){
						String party = wordsarray[2];
						votemachine.addParty(party);
						System.out.println("You added the party " + party);
						run = true;
					} else {
						System.out.println("This command was unvalid");
						run = true;
					}
				}
				
				else if(wordsarray[0].equals("VOTES")){
					if (wordsarray.length == 1){
						System.out.println("Votes: ");
						this.showVotes(votemachine.votelist.getVotes());
						run = true;
					} else {
						System.out.println("This command was unvalid");
						run = true;
					}
				}
				
				else if(wordsarray[0].equals("PARTIES")){
					if (wordsarray.length == 1){
						System.out.println("Parties: ");
						this.showParties(votemachine.partylist.getParties());
						run = true;
					} else {
						System.out.println("This command was unvalid");
						run = true;
					}
				}
				
				else if(wordsarray[0].equals("EXIT")){
					System.out.println("Are you sure you want to exit? y/n");
					String answer = scanner.nextLine();
					if (answer.equals("y")){
						System.out.println("You are leaving the system.");
						run = false;
					} else if(answer.equals("n")){
						System.out.println("You are returning to the menu");
						run = true;
					} else {
						run = true;
					}
						
				}
				
				else if(words.equals("HELP")){
					System.out.println("Whelp, there is no help-function implemented");
					System.out.println("You are returning to the menu");
					run = true;
				}
					
		}
		scanner.close();
	}
	
	public void showVotes(Map<String, Integer> votes){
		Set<String> parties = votes.keySet();
		for (String party : parties){
			System.out.println("Party: " + party + " has " +votes.get(party) + " votes.");
		}
	}
	
	public void showParties(List<String> parties){
		for(int i = 0; i < parties.size(); i++){
			System.out.println("Party: " + parties.get(i));
		}
	}
	
	public void showError(String error){
		System.out.print(error);
	}
	
	public void getParties(){
		
	}

	@Override
	public void update(Observable arg0, Object arg1) {
		if (arg1 instanceof String){
			if (arg1.equals("Party")){
				System.out.println("Party has been added.");
			} else if (arg1.equals("Vote")){
				System.out.println("Vote has been added");
			}
		}
	}
}
