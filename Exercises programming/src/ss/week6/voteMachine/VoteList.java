package ss.week6.voteMachine;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.Observable;

public class VoteList extends Observable{
	Map<String,Integer> voteList = new HashMap<String,Integer>();
		
	
	public VoteList(Map<String,Integer> votelist) {
		this.voteList = votelist;
	}
	
	public VoteList(){
	}
	
	public void addVote(String name){
		int voteCounter = 0;
			if (voteList.keySet().contains(name)){
				voteCounter = voteList.get(name);
			}
		
		voteList.remove(name, voteCounter);
		voteList.put(name, voteCounter +1);
		super.setChanged();
		super.notifyObservers("Vote");
	}
	
	public Map<String,Integer> getVotes(){
		return voteList;
	}
	
}


