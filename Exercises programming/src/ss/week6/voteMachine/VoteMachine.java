package ss.week6.voteMachine;
import java.util.ArrayList;
import ss.week6.voteMachine.gui.VoteGUIView;


public class VoteMachine {
	PartyList partylist;
	VoteList votelist;
	VoteView voteView;
	
	public VoteMachine(){
		votelist = new VoteList();
		partylist = new PartyList();
		//VoteTUIView voteTuiView = new VoteTUIView(this);
		//partylist.addObserver(voteTuiView);
		//votelist.addObserver(voteTuiView);		

		voteView = new VoteGUIView(this);
		partylist.addObserver(voteView);
		votelist.addObserver(voteView);
	}
	
	
	public VoteMachine(PartyList plist, VoteList vlist) {
		partylist = plist;
		votelist = vlist;
		//VoteTUIView voteTuiView = new VoteTUIView(this);
		//partylist.addObserver(voteTuiView);
		//votelist.addObserver(voteTuiView);		
		

		voteView = new VoteGUIView(this);
		partylist.addObserver(voteView);
		votelist.addObserver(voteView);
	}
	
	public void start(){
		//voteTuiView.start();
		voteView.start();
	}
	
	public static void main(String[] args){
		VoteMachine votemachine = new VoteMachine();
		votemachine.start();
	}
	
	public void addParty(String party){
		partylist.addParty(party);
	}
	
	public void vote(String party){
		votelist.addVote(party);
		
	}
	
	public void getParties(){
		ArrayList<String> parties = partylist.getParties();
		voteView.showParties(parties);
	}
}
