package ss.week6.voteMachine;

import java.util.ArrayList;
import java.util.Observable;

public class PartyList extends Observable{
	ArrayList<String> partyList;
		
	
	public PartyList(ArrayList<String> list) {
		partyList = list;
	}
	
	public PartyList(){
		partyList = new ArrayList<String>();
	}
	
	public void addParty(String name){
		partyList.add(name);
		super.setChanged();
		super.notifyObservers("Party");
	}
	
	public ArrayList<String> getParties(){
		return partyList;
	}
	
	public Boolean hasParty(String name){
		return(partyList.contains(name));
		
	}
	
}
