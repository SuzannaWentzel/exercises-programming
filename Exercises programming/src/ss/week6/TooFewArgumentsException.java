package ss.week6;

public class TooFewArgumentsException extends WrongArgumentException {

	/**
	 * 
	 * 
	 */
	
	public TooFewArgumentsException(int s1, int s2){
		super("ERR to few arguments: " + (s1 - s2) + " to few.");//
	}
	
	public TooFewArgumentsException(){
		super("ERR to few arguments");
	}
	
	private static final long serialVersionUID = 160286419303419282L;

	public String getMessage(){
		return "ERR to few arguments";
	}

}
