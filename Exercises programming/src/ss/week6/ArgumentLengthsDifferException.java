package ss.week6;

public class ArgumentLengthsDifferException extends WrongArgumentException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5762222081745227107L;
	/**
	 * 
	 */

	public ArgumentLengthsDifferException(int s1, int s2){
		super("ERR arguments length differ");
	}
	
	public ArgumentLengthsDifferException(){
		super("ERR arguments length differ");
	}


	public String getMessage(){
		return "ERR arguments length differ";
	}

}
