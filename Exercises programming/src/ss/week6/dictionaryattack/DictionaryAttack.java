package ss.week6.dictionaryattack;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Map;
import java.util.Set;

import org.apache.commons.codec.binary.Hex;

import java.security.*;
import java.math.*;
import java.util.HashMap;



public class DictionaryAttack {
	private Map<String, String> passwordMap;
	private Map<String, String> hashDictionary;
	private static final String PATH = ""; //Your path to the test folder

	/**
	 * Reads a password file. Each line of the password file has the form:
	 * username: encodedpassword
	 * 
	 * 
	 *\
	 *
	 
	 * After calling this method, the passwordMap class variable should be
	 * filled withthe content of the file. The key for the map should be
	 * the username, and the password hash should be the content.
	 * @param filename
	 */
	
	public DictionaryAttack(){
		passwordMap = new HashMap<String,String>();
		hashDictionary = new HashMap<String,String>();
		
		
	}
	
	public void readPasswords(String filename) {
		String thisline = null;
		
		try {
			BufferedReader reader = new BufferedReader(new FileReader(PATH + filename));
			while ((thisline = reader.readLine()) != null) {
				String[] splitted = thisline.split(": ");
				passwordMap.put(splitted[0], splitted[1]);
			}
		reader.close();    	
		} 	 
		catch (IOException e1) {
			e1.printStackTrace();	
		}
         
	}

	/**
	 * Given a password, return the MD5 hash of a password. The resulting
	 * hash (or sometimes called digest) should be hex-encoded in a String.
	 * @param password
	 * @return 
	 */
	public String getPasswordHash(String password) {
    		
	try{
	    MessageDigest m=MessageDigest.getInstance("MD5");
	    return (Hex.encodeHexString(m.digest(password.getBytes())));
	}
	catch (Exception e){
		System.out.println("Error");
	}
	return null;
    		
	}
	/**
	 * Checks the password for the user the password list. If the user
	 * does not exist, returns false.
	 * @param user
	 * @param password
	 * @return whether the password for that user was correct.
	 */
	public boolean checkPassword(String user, String password) {
        
	return(passwordMap.containsKey(user) && 
			passwordMap.get(user).equals(getPasswordHash(password)));
		
	}

	/**
	 * Reads a dictionary from file (one line per word) and use it to add
	 * entries to a dictionary that maps password hashes (hex-encoded) to
     * the original password.
	 * @param filename filename of the dictionary.
	 */
	public void addToHashDictionary(String filename) {
		String thisline = null;
		try{
			BufferedReader reader = new BufferedReader(new FileReader("C:" + File.separator + "user\\" + filename));
			while ((thisline = reader.readLine()) != null) {
				hashDictionary.put(getPasswordHash(thisline), thisline);
			}
			reader.close();
		}
		catch(Exception e){
			System.out.println("error");
		}
		
    // To implement        
	}
	/**
	 * Do the dictionary attack.
	 */
	public void doDictionaryAttack() {
		this.addToHashDictionary("MostCommonPasswords.txt");
		Set<String> keySetHashDictionary = hashDictionary.keySet();
		Set<String> keySetPasswordMap = passwordMap.keySet();
		for (String user : keySetPasswordMap){
			if(keySetHashDictionary.contains(passwordMap.get(user))){
				System.out.println("Name: " + user + ", password: " + hashDictionary.get(passwordMap.get(user)));
			}
		}
	}
	
	public static void main(String[] args) {
		DictionaryAttack da = new DictionaryAttack();
		da.readPasswords("C:" + File.separator + "user\\" + "LeakedPasswords.txt");
		da.doDictionaryAttack();
	}

}
