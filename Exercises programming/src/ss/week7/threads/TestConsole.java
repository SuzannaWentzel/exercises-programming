package ss.week7.threads;

import java.util.Scanner;

public class TestConsole extends Thread {
	String name;
	
	public TestConsole(String name) {
		// TODO Auto-generated constructor stub
	}
	
	public TestConsole(){
		
	}
	
	private void sum(){
		int integer1 = Console.readInt("Can you enter an integer?");
		int integer2 = Console.readInt("Can you enter another integer?");
		int sum = integer1 + integer2;
		Console.println("Thread " + this.getName() + ": get number 1? " + integer1);
		Console.println("Thread " + this.getName() + ": get number 2? " + integer2);
		Console.println("Thread " + this.getName() + ": " + integer1 + " + " + integer2 + " = " + sum);
		
		
	}
	
	public void run(){
		sum();
	}
}
