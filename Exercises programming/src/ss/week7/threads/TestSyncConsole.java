package ss.week7.threads;

public class TestSyncConsole extends Thread{
String name;
	
	public TestSyncConsole(String name) {
		// TODO Auto-generated constructor stub
	}
	
	public TestSyncConsole(){
		
	}
	
	private synchronized void sum(){
		int integer1 = SyncConsole.readInt("Can you enter an integer?");
		int integer2 = SyncConsole.readInt("Can you enter another integer?");
		int sum = integer1 + integer2;
		SyncConsole.println("Thread " + this.getName() + ": get number 1? " + integer1);
		SyncConsole.println("Thread " + this.getName() + ": get number 2? " + integer2);
		SyncConsole.println("Thread " + this.getName() + ": " + integer1 + " + " + integer2 + " = " + sum);
		
		
	}
	
	public synchronized void run(){
		sum();
		
	}
}
