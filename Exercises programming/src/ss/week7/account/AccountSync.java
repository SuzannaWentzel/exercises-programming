package ss.week7.account;

public class AccountSync {

	public static void main(String[] args) {
		
		Account Suzanna = new Account();
		Thread t1 = new MyThread(20.00, 30000, Suzanna);
		Thread t2 = new MyThread(-20.00 , 30000, Suzanna);
		Thread t3 = new MyThread(20.00, 3, Suzanna);
		
		t1.start();
		t2.start();
		
		try {
			t1.join();
			t2.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		System.out.println(Suzanna.getBalance());
	}

}
