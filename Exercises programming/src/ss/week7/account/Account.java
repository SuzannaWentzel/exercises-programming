package ss.week7.account;

public class Account {
	private double balance = 0.0;

	public synchronized void transaction(double amount) {
		double optBalance = balance + amount;
		while (optBalance < -1000.00){
			try {
				wait();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		balance = optBalance;
		notifyAll();
	}
	public double getBalance() {
		return balance;

	}
}
