package ss.week7.cmdline;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.Socket;

import com.sun.prism.shader.Solid_TextureYV12_AlphaTest_Loader;

/**
 * Peer for a simple client-server application
 * @author  Theo Ruys
 * @version 2005.02.21
 */
public class Peer implements Runnable {
    public static final String EXIT = "exit";

    protected String name;
    protected Socket sock;
    protected BufferedReader in;
    protected BufferedWriter out;


    /*@
       requires (nameArg != null) && (sockArg != null);
     */
    /**
     * Constructor. creates a peer object based in the given parameters.
     * @param   nameArg name of the Peer-proces
     * @param   sockArg Socket of the Peer-proces
     */
    public Peer(String nameArg, Socket sockArg) throws IOException {
    	name = nameArg;
    	sock = sockArg;
    	try{
    		in = new BufferedReader(new InputStreamReader(sock.getInputStream()));
    		out = new BufferedWriter(new OutputStreamWriter(sock.getOutputStream()));
    	}
    	catch(IOException e){
    		System.out.println("Unable to open reader/writer");
    	}
    }

    /**
     * Reads strings of the stream of the socket-connection and
     * writes the characters to the default output.
     */
    public void run() {
    	try{
    		while(true){
    			String input = in.readLine();
    			if (input == null){
    				System.out.println("Connection closed");
    				shutDown();
    				break;
    			}
    			System.out.println(input);
    		}
    	}
    	catch(IOException e){
    		System.out.println("Connection closed");
    	}
    }


    /**
     * Reads a string from the console and sends this string over
     * the socket-connection to the Peer process.
     * On Peer.EXIT the method ends
     */
    public void handleTerminalInput() {
    	while (true){
    		String input = readString("");
    		if (input.equals("EXIT")){
    			shutDown();
    			break;
    		}
    		try{
    			out.write(name + " " + input);
    			out.newLine();
    			out.flush();
    		}
    		catch(IOException e){
    			System.out.println("Could not write to string");
    		}
    	}
    }

    /**
     * Closes the connection, the sockets will be terminated
     */
    public void shutDown() {
    	try{
    		sock.close();
    		out.close();
    		in.close();
    	}
    	catch(IOException e){
    		e.printStackTrace();
    		System.out.println("Could not close");
    	}
    }

    /**  returns name of the peer object*/
    public String getName() {
        return name;
    }

    /** read a line from the default input */
    static public String readString(String tekst) {
        System.out.print(tekst);
        String antw = null;
        try {
            BufferedReader in = new BufferedReader(new InputStreamReader(
                    System.in));
            antw = in.readLine();
        } catch (IOException e) {
        }

        return (antw == null) ? "" : antw;
    }
}
