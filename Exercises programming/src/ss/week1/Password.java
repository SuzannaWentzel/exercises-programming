package ss.week1;

public class Password {
	/**
	 * The standard initial password.
	 */
	public static final String INITIAL = "miloisgeweldig123enchrisook";
	private String pass;
	
	/**
	 * Constructs a password with the initial word provided in INITIAL.
	 */
	public Password(){
		 pass = INITIAL;
	}
	
	/**
	 * Tests if a string is a valid password.
	 * @param suggestion Word that should be tested
	 * @return true if suggestion is acceptable 
	 */
	public boolean acceptable(String suggestion){
		return (suggestion.length() > 6 && !suggestion.contains(" "));
	}
	
	/**
	 * Changes the password
	 * @param oldpass The current password
	 * @param newpass The new password
	 * @return true if old is equal to the current password and that newpass is an acceptable password
	 */
	public boolean setWord(String oldpass, String newpass){
		if (oldpass.equals(this.pass) && acceptable(newpass)){
			this.pass = newpass;
			return true;
		}
		return false;
	}
	
	/**
	 * Tests if a given word is equal to the current password.
	 * @param test Word that should be tested
	 * @return true If test is equal to the current password
	 */
	public boolean testWord(String test){
		return (test.equals(this.pass));
	}
}
