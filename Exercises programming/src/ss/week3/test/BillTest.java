package ss.week3.test;
import ss.week3.hotel.Bill;
import ss.week3.hotel.Item;

import static org.junit.Assert.*;


import org.junit.Test;
import org.junit.Before;

public class BillTest {
	Bill bill, billNull;
	
	@Before
	public void setup(){
		bill = new Bill(System.out);
		billNull = new Bill(null);
	}
	
	
	@Test
	public void testClose() {
		bill.newItem(new Item("Apple", 0.49));
		bill.close();
		billNull.newItem(new Item("Orange", 0.57));
		billNull.close();
	}
	
	@Test
	public void testGetSum(){
		assertEquals(0, bill.getSum(), 0);
		bill.newItem(new Item("Apple", 0.49));
		assertEquals(0.49, bill.getSum(), 0);
		bill.newItem(new Item("Orange", 0.57));
		assertEquals(1.06, bill.getSum(), 0);
		
		assertEquals(0, billNull.getSum(), 0);
		billNull.newItem(new Item("Banana", 0.31));
		assertEquals(0.31, billNull.getSum(), 0);
		billNull.newItem(new Item("Pear", 0.42));
		assertEquals(0.73, billNull.getSum(), 0);	
	}
	
	@Test
	public void testNewItem(){
		bill.newItem(new Item("Apple", 0.49));
		billNull.newItem(new Item("Orange", 0.57));
	}
}
