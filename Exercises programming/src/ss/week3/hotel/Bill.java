package ss.week3.hotel;

import java.io.PrintStream;

public class Bill {
	private PrintStream theOutStream;
	private double sum;
	
	public interface Items {
		public double getValue();
	}
	
	public Bill(PrintStream theOutStream) {
		this.theOutStream = theOutStream;
	}

	public void close(){
		if (theOutStream == null) return;
		theOutStream.println(sum);
	}
	
	/*@ pure */ public double getSum(){
		return sum;
	}
	
	public void newItem(Bill.Items item){
		if (theOutStream != null) theOutStream.println(item.toString());
		sum = sum + item.getValue();
	}

}
