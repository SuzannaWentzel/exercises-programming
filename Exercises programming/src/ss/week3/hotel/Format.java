package ss.week3.hotel;

public class Format {

	public static void main(String[] args) {
		printLine("halloo", 3.3);
		printLine("halloo", 23.34);
	}
	
	public static void printLine(String text, double amount){
		System.out.printf("%-20s %8.2f \n", text, amount);
	}

}
