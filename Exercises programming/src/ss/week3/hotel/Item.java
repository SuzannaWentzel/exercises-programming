package ss.week3.hotel;

public class Item implements Bill.Items {
	private String name;
	private double price;
	
	public Item(String name, double price) {
		this.name = name;
		this.price = price;
	}

	@Override
	public double getValue() {
		return price;
	}

	@Override
	public String toString() {
		return name;
	}
}
