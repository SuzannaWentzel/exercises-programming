package ss.week3.hotel;

public class PricedRoom extends Room implements Bill.Items {
	private double roomPrice;

	public PricedRoom(int no, double roomPrice, int safePrice) {
		super(no, new PricedSafe(safePrice));
		this.roomPrice = roomPrice;
	}

	@Override
	public double getValue() {
		return roomPrice;
	}
	
	@Override
	public String toString(){
		return Double.toString(roomPrice);
	}

}
