package ss.week3.hotel;

public class PricedSafe extends Safe implements Bill.Items {
	private double price;
	
	public PricedSafe(double price) {
		super(new Password());
		this.price = price;
	}

	@Override
	public double getValue() {
		return this.price;
	}
	
	@Override
	public String toString(){
		return Double.toString(price);
	}

}
