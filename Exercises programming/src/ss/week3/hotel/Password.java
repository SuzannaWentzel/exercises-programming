package ss.week3.hotel;
import ss.week3.Checker;
import ss.week3.BasicChecker;
/**
 * 
 * @author Suzanna Wentzel
 *
 */

public class Password {
	/**
	 * The standard initial password.
	 */
	private String pass;
	public Checker checker;
	public String factoryPassword;
	
	/**
	 * Constructs a password with the initial word provided in INITIAL and changes the <code>Checker</code> to <code>BasicChecker</code>..
	 */
	public Password(){
		this(new BasicChecker());
	}
	
	/**
	 * Constructs a password which suffices to the checker.
	 * @param checker
	 */
	public Password(Checker checker){
		this.checker = checker;
		this.factoryPassword = checker.generatePassword();
		this.pass = this.factoryPassword;
	}
	
	
	/**
	 * Changes the password
	 * @param oldpass The current password
	 * @param newpass The new password
	 * @return true if old is equal to the current password and that newpass is an acceptable password
	 */
	public boolean setWord(String oldpass, String newpass){
		if (oldpass.equals(this.pass) && checker.acceptable(newpass)){
			this.pass = newpass;
			return true;
		}
		return false;
	}
	
	/**
	 * Tests if a given word is equal to the current password.
	 * @param test Word that should be tested
	 * @return true If test is equal to the current password
	 */
	/*@ pure */ public boolean testWord(String test){
		return (test.equals(this.pass));
	}
}
