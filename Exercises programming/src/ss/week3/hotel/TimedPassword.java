package ss.week3.hotel;
import java.lang.*;
/**
 * 
 * @author Suzanna Wentzel
 *
 */

public class TimedPassword extends Password {
	public static long VALIDTIME;
	public static long startTime;
	public static long defaultValidtime = 4200000 ;
	
	
	public TimedPassword (long validTime){
		startTime = System.currentTimeMillis();
		VALIDTIME = validTime * 1000;
		
	}
	
	public TimedPassword (){
		VALIDTIME = defaultValidtime;
	}
	
	public boolean isExpired() {
		return (System.currentTimeMillis() - VALIDTIME > startTime);
	}
	
	public void reset(){
		this.startTime = System.currentTimeMillis();
	}
	
	@Override
	public boolean setWord(String oldpass, String newpass) {
		reset();
		return super.setWord(oldpass, newpass);
	}
}