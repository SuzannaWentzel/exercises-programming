package ss.week3;

public class StrongChecker extends BasicChecker {
	public static final String INITIAL = "Applepie3";
	public String pass;
	
	@Override
	public boolean acceptable(String password) {
		return (super.acceptable(password) 
				&& Character.isLetter(password.charAt(0)) 
				&& Character.isDigit(password.charAt(password.length()-1)) );
	}
	
	public String generatePassword(){
		return INITIAL;
	}

}
