package ss.week5;

public class ComputerPlayer extends Player {
	Strategy strategy;
	Mark mark;
	
	public ComputerPlayer(Mark mark, Strategy strategy) {
		super(mark + "-" + strategy, mark);	
		this.strategy = strategy;
	}
	
	public ComputerPlayer(Mark mark){
		super(mark + "-" + "Naive", mark);
		this.strategy = new NaiveStrategy();
	}
	
	public int determineMove(Board board){
		return strategy.determineMove(board, mark);
	}
	
	public void updateStrategy(Strategy strategy){
		this.strategy = strategy;
	}

}
