package ss.week5;

import ss.week5.Board;
import ss.week5.Mark;

import java.util.Collection;
import java.util.HashMap;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;


public class NaiveStrategy implements Strategy {

	@Override
	public String getName() {
		return "Naive";
	}

	@Override
	public int determineMove(Board b, Mark m) {
		Set <Integer> fields = new HashSet<Integer>();
		for(int i = 0; i < 9; i++){
			fields.add(i);
		}
		
		double index = (Math.random()*(fields.size()));
		int indexResult = (int) index;
		
		if (b.isField(indexResult) && b.getField(indexResult)==Mark.EMPTY){
			return indexResult;
		}
		else return determineMove(b, m);
	}
}
