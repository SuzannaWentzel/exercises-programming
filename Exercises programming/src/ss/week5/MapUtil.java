package ss.week5;

import java.util.Collection;
import java.util.HashMap;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;



public class MapUtil {
	
	//@ requires map != null
	//@ ensures \result == true | \result == false
	public static <K, V> boolean isOneOnOne(Map<K, V> map) {		
		assert (map != null);
		Collection<V> valuesList = map.values();
		Set<V> valuesSet = new HashSet<V>(map.values());
		return valuesList.size() == valuesSet.size();
	}
	
    //@ requires map != null
	//@ ensures \result == true | \result == false
	public static <K, V> boolean isSurjectiveOnRange(Map<K, V> map, Set<V> range) {
    	assert (map != null);
		for (V v : range) {
    		if (!map.containsValue(v)) {
    			return false;
    		}
    	}
    	return true;
    }
    
	//@ requires map != null
    public static <K, V> Map<V, Set<K>> inverse(Map<K, V> map) {
    	assert(map != null);
    	Map<V, Set<K>> result = new HashMap<V,Set<K>>();
    	Set <K> setOfKeys = map.keySet();
    	for (K k: setOfKeys){
    		V newKey = map.get(k);
    		if (result.containsKey(newKey)){
    			result.get(newKey).add(k);    		}
    		else {
    			Set <K> resultKeys = new HashSet<K>(); 
    			resultKeys.add(k);
    			result.put(newKey, resultKeys);
    		}
    	}
    	return result;
	}
    
    //@ requires map != null
	public static <K, V> Map<V, K> inverseBijection(Map<K, V> map) {
       	assert (map != null);
		Set <K> setOfKeys = map.keySet();
		Map<V, K> result = new HashMap <V, K>();
		
		if(isOneOnOne(map)){
			for (K k : setOfKeys){
				V newKey = map.get(k);
				result.put(newKey, k);
			}
			return result;
        }
		else return null;
	}
	
	//@ requires f != null && g != null
	public static <K, V, W> boolean compatible(Map<K, V> f, Map<V, W> g) {
		assert(f != null && g != null);
		Set <K> keysFunction1 = f.keySet();
		Boolean result = true;
		
		for (K k : keysFunction1){
			if (g.containsKey(f.get(k))){
				result = true;
			}
			else {
				return false;
			}
		}
		return result;
	}
	
	//@ requires f != null && g != null
	//@ requires compatible(f,g)
	public static <K, V, W> Map<K, W> compose(Map<K, V> f, Map<V, W> g) {
		assert (f != null && g != null);
		Map <K,W> result = new HashMap<K,W>();
		Set <K> keysF = f.keySet();
		if (compatible(f,g)){
			for (K k : keysF){
				K newKey = k;
				W newValue = g.get(f.get(k));
				result.put(newKey, newValue);
			}
			return result;
		}
		else {
			return null;
		}
	}
}
