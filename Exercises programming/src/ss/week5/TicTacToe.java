package ss.week5;

import ss.week5.Game;
import ss.week5.HumanPlayer;
import ss.week5.Mark;
import ss.week5.NaiveStrategy;

/**
 * Executable class for the game Tic Tac Toe. The game can be played against the
 * computer. Lab assignment Module 2
 * 
 * @author Theo Ruys
 * @version $Revision: 1.4 $
 */
public class TicTacToe {
    public static void main(String[] args) {
    	
    	if (args[0].equals("-S") && args[1].equals("-N")){
    		Game game = new Game(new ComputerPlayer(Mark.OO, new NaiveStrategy()),new ComputerPlayer(Mark.XX, new SmartStrategy()) );
    		game.start();
    	}
    	
    	else if (args[1].equals("-S")){
    		Game game = new Game(new ComputerPlayer(Mark.OO, new SmartStrategy()), new HumanPlayer(args[0], Mark.XX));
    		game.start();
    		
    	}
    	
    	else if (args[1].equals("-N")){
    		Game game = new Game(new ComputerPlayer(Mark.OO, new NaiveStrategy()), new HumanPlayer(args[0], Mark.XX));    	
    		game.start();
    	}
    	
    	else{
    		Game game = new Game(new HumanPlayer(args[0], Mark.OO), new HumanPlayer(args[1], Mark.XX));    	
    		game.start();
    	}
    	
    	
    	
    }
}