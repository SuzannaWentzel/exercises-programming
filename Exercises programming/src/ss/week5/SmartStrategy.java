package ss.week5;

import java.util.HashSet;
import java.util.Set;

public class SmartStrategy implements Strategy {


	@Override
	public String getName() {
		return "Smart";
	}

	@Override
	public int determineMove(Board b, Mark m) {
		Set <Integer> fields = new HashSet<Integer>();
		Board copy = b.deepCopy();
		
		Mark opponent = (m == Mark.XX) ? Mark.OO : Mark.XX;
		
		for(int i = 0; i < 9; i++){
			fields.add(i);
		}
		
		if (b.getField(4) == Mark.EMPTY){
			return 4;
		}
		else {
			for (Integer f : fields){
				if (b.getField(f) == Mark.EMPTY){
					copy.setField(f, m);
					if (copy.isWinner(m)){
						copy.setField(f, Mark.EMPTY);
						return f;
					}
				}
				copy = b.deepCopy();
			}
			
			for (Integer f : fields){
				if (b.getField(f) == Mark.EMPTY){
					copy.setField(f, opponent);
					if (copy.isWinner(opponent)){
						return f;
					}
				}
				copy = b.deepCopy();
			}
						
			
			double index = (Math.random()*(fields.size()));
			int indexResult = (int) index;
			
			
			if (b.isField(indexResult) && b.getField(indexResult)==Mark.EMPTY){
				return indexResult;
			} 
			else{
				while (b.getField(indexResult) != Mark.EMPTY){
					index = (Math.random()*(fields.size()));
				}
				indexResult = (int) index;
				return indexResult;
			}
		}
		
	}
	
}
