package ss.week5;

import org.apache.commons.codec.DecoderException;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.binary.Hex;


/**
 * 
 * A simple class that experiments with the Hex encoding
 * of the Apache Commons Codec library.
 *
 */
public class EncodingTest {
    public static void main(String[] args) throws DecoderException {
        //Hex
    	String input = "Hello World";
    	String innput = "4d6f64756c652032";
        String input2 = "010203040506";
        String input3 = "U29mdHdhcmUgU3lzdGVtcw==";
        String input4 = "a";
        String input5 = "aa";
        String input6 = "aaa";
        String input7 = "aaaa";
        String input8 = "aaaaa";
        String input9 = "aaaaaa";
        String input10 = "aaaaaaa";
        String input11 = "aaaaaaaa";
        String input12 = "aaaaaaaaa";
        String input13 = "aaaaaaaaaa";

        byte[] bytearray = Hex.decodeHex(innput.toCharArray());
        byte[] bytearray2 = Base64.decodeBase64(input2);

        
        //11
        System.out.println(Hex.encodeHexString(input.getBytes()));
        
        //12        
        System.out.println(new String(bytearray));
        
        //13
        System.out.println(new String(Base64.encodeBase64(input.getBytes())));
        System.out.println(new String(Base64.encodeBase64(bytearray2)));
        System.out.println(new String(Base64.decodeBase64(input3)));
        System.out.println(new String(Base64.encodeBase64(input4.getBytes())));
        System.out.println(new String(Base64.encodeBase64(input5.getBytes())));
        System.out.println(new String(Base64.encodeBase64(input6.getBytes())));
        System.out.println(new String(Base64.encodeBase64(input7.getBytes())));
        System.out.println(new String(Base64.encodeBase64(input8.getBytes())));
        System.out.println(new String(Base64.encodeBase64(input9.getBytes())));
        System.out.println(new String(Base64.encodeBase64(input10.getBytes())));
        System.out.println(new String(Base64.encodeBase64(input11.getBytes())));
        System.out.println(new String(Base64.encodeBase64(input12.getBytes())));
        System.out.println(new String(Base64.encodeBase64(input13.getBytes())));

        
        
        
        //System.out.println()
        
        
        
        /*    
        String input3 = "Hello World";
        String input4 = "010203040506";
        String input5 = "U29mdHdhcmUgU3lzdGVtcw==";
        byte[] a = Base64.getDecoder().decode(input4);
        byte[] b = Hex.decodeHex(input2.toCharArray());


        System.out.println(new String(Base64.getDecoder().decode(input5.getBytes())));
        System.out.println(new String(Base64.getEncoder().encode("a".getBytes())));
        System.out.println(new String(Base64.getEncoder().encode("aa".getBytes())));
        System.out.println(new String(Base64.getEncoder().encode("aaa".getBytes())));
        System.out.println(new String(Base64.getEncoder().encode("aaaa".getBytes())));
        System.out.println(new String(Base64.getEncoder().encode("aaaaa".getBytes())));
        System.out.println(new String(Base64.getEncoder().encode("aaaaaa".getBytes())));
        System.out.println(new String(Base64.getEncoder().encode("aaaaaaa".getBytes())));
        System.out.println(new String(Base64.getEncoder().encode("aaaaaaaa".getBytes())));
        System.out.println(new String(Base64.getEncoder().encode("aaaaaaaaa".getBytes())));
        System.out.println(new String(Base64.getEncoder().encode("aaaaaaaaaa".getBytes())));
    }
}*/
        
        
        
      
        
        
        
        
        
    
    }
}
