package ss.week2.hotel;

import ss.week2.hotel.Password;
/**
 * 
 * @author Suzanna Wentzel
 *
 */

public class Safe {
	public boolean safeActivated;
	public boolean safeOpened;
	
	public Password password;
	int safeNumber;
	
	/**
	 * Constructs an instance of Safe.
	 * @param safeNumber
	 * @param code
	 */
	//@ ensures safeNumber >= 0;
	//@ ensures code != null;
	public Safe(int safeNumber, String code) {
		assert code != null && safeNumber >= 0;
		this.safeNumber = safeNumber;
		password = new Password();
		password.setWord(password.INITIAL, code);
	}
	
	/**
	 * Activates the <code>Safe</code> if the password is correct
	 * @param password
	 */
	//@ requires code != null;
	//@ ensures password.testWord(code) ==> isActive();
	public void activate(String code) {
		assert code != null;
		if (password.testWord(code)){
			safeActivated = true;
			System.out.println("This safe is activated");
		}
	}
	
	/**
	 * Closes the <code>Safe</code> and deactivates it.
	 */
	//@ ensures !isActive() && !isOpen();
	public boolean deactivates() {
		safeActivated = false;
		System.out.println("This safe is deactivated");
		return !safeActivated;
	}
	
	/**
	 * Opens the <code>Safe</code> if it is active and if the password is correct.
	 * @param password
	 */
	//@ requires password.testWord(code) == true;
	//@ requires safeActivated == true;
	//@ ensures safeOpened == true;
	public void open(String code) {
		assert code != null;
		if (password.testWord(code) && safeActivated){
			safeOpened = true;
			System.out.println("This safe is opened");
		}
	}
	
	/**
	 * Closes the <code>Safe</code>.
	 */
	//@ ensures safeOpened == false; 
	public void close() {
		safeOpened = false;
		System.out.println("This safe is closed");
	}
	
	/**
	 * Indicates whether the <code>Safe</code> is active.
	 * @return boolean that says if it is active or not.
	 */
	//@ ensures \result == safeActivated;
	/*@ pure */ public boolean isActive() {
		return (safeActivated);
	}
	
	/**
	 * Indicates whether the <code>Safe</code> is open.
	 * @return boolean that says if it is open or not.
	 */
	//@ ensures \result == safeOpened;
	/*@ pure */ public boolean isOpen() {
		return (safeOpened);
	}
	
	/**
	 * Returns the string password.getPassword().
	 * @return password object
	 */
	//@ ensures \result == password.getPassword();
	/*@ pure */ public String getPassword() {
		return password.getPassword();
	}
	
	public String safeStatus(){
		if (isActive() && isOpen()){
			return "Safe is activated and open.";
		} else if (isActive() && !isOpen()){
			return "Safe is activated and closed";
		} else {
			return "safe is unactive";
		}
	}
	public static void main(String[] args){
			Safe newSafe = new Safe(-1,"ABCDEFGH");
				System.out.println("succeeded");
	}	
}
