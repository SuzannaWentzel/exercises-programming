package ss.week2.hotel;

/**
 * Hallooooo sick :)
 * @author Suzanna Wentzel
 *
 */
public class Hotel {
	public String hotelName;
	public Room room1;
	public Room room2;
	public Password password;
	
	/**
	 * Constructor for a <code>Hotel</code>
	 * @param name Name of guest
	 */
	//@ requires name != null;
	//@ ensures hotelName == name;
	//@ ensures room1 == new Room(100) && room2 == new Room(101) && password == new Password();
	public Hotel(String name){
		hotelName = name;
		room1 = new Room(100);
		room2 = new Room(101);
		password = new Password();
	}
	
	/**
	 * Checks the <code>Guest</code> in to the <code>Hotel</code>
	 * @param givenPassword Inserted password
	 * @param guestName Name of the <code>Guest</code>
	 * @return null if the password was incorrect or if the person already exists, else returns <code>room</code> where checked in
	 */
	//@ requires givenPassword != null && guestName != null;
	public Room checkIn(String givenPassword, String guestName){
		
		if (!password.testWord(givenPassword)){
			return null;
		} if (getRoom(guestName) != null){
			return null;
		} Room result = getFreeRoom();
		if (result != null){
			Guest guest = new Guest(guestName);
			guest.checkin(result);	
		}
			
		return result;
		
	}
	
	/**
	 * Checks a <code>Guest</code> out
	 * @param guestName Name of <code>Guest</code>
	 */
	//@ requires guestName != null;
	//@ requires getRoom(guestName) != null;
	//@ ensures getRoom(guestName).getGuest().checkout();
	//@ ensures getRoom(guestName).getSafe().deactivates();
	public void checkOut(String guestName){
		Room r = getRoom(guestName);
		if (r != null){
			r.getGuest().checkout();
			r.getSafe().deactivates();
		}
	}
	
	/**
	 * Finds a free <code>Room</room>
	 * @return Free <code>Room</room>
	 */
	/*@ pure */ public Room getFreeRoom(){
		if(room1.getGuest() == null){
			return room1;
		}
		if(room2.getGuest() == null){
			return room2;
		}
		else return null;
	}
	
	/**
	 * Returns the room of a <code>Guest</code>
	 * @param guestName Name of <code>Guest</code>
	 * @return <code>Room</code> of <code>Guest</code>
	 */
	//@ requires guestName != null;
	/*@ pure */ public  Room getRoom(String guestName){
		Guest guest = room1.getGuest();
		
		if (guest != null && guest.getName().equals(guestName)){
			return room1;
		}
		guest = room2.getGuest();
		
		if (guest != null && guest.getName().equals(guestName)){
			return room2;
		}
		else {
			return null;
		}
	}
	
	/**
	 * Get password
	 * @return password
	 */
	/*@ pure */ public Password getPassword(){		
	return this.password;
	}
	
	/**
	 * Gives information about the <code>Room</code>, <code>Guest</code> and <code>Safe</code>.
	 */
	/*@ pure*/ public String toString() {
		    String string = "";
		    if (room1.getGuest() != null) {
		        string += room1.toString() + room1.getGuest().toString() + room1.getSafe().toString();
		    } else {
		        string += room1.toString() + "Guest: None" + room1.getSafe().toString();
		    }

		    if (room2.getGuest() != null) {
		        string += room2.toString() + room2.getGuest().toString() + room2.getSafe().toString();
		    } else {
		        string += room2.toString() + "Guest: None" + room2.getSafe().toString();
		    }

		    return string;
	}
}
