package ss.week2.hotel;

/**
 * Guest of hotel
 * @version 0.0.5
 * @author Suzanna Wentzel
 * 
 */
public class Guest {
	
	private String name;
	private Room room;
	
	/**
	 * Returns a description <code>Guest</code>, which gives the name of the guest.
	 */
	public String toString(){
		return "Guest: " + name;
	}
	
	/**
	 * Create new <code>Guest</code> with name
	 * @param name Name of the <code>Guest</code>
	 */
	public Guest(String name){
		this.name = name;
	}
	
	/**
	 * Check <code>Guest</code> in into <code>Room</code> r
	 * @param r Room to be checked in into
	 * @return true if checkin was successfull, false otherwise
	 */
	public boolean checkin(Room r){
		if (r.getGuest() == null && room == null){
			room = r;
			room.setGuest(this);
			return true;
		} 
		else{return false;}
	}
	
	/**
	 * Check <code>Guest</code> out of <code>Room</code>
	 * @return true if checkout was succesfull, false otherwise
	 */
	public boolean checkout(){
		if (room != null && room.getGuest() != null){
			room.setGuest(null);
			room = null;
			return true; 
		}
		else {
			return false;
		}
		
	}
	/**
	 * Get name of <code>Guest</code>
	 * @return name of <code>Guest</code>
	 */
	public String getName(){
		return name;
	}
	
	/**
	 * Get <code>Room</code> of <code>Guest</code>
	 * @return <code>Room</code> of <code>Guest</code>
	 */
	public Room getRoom(){
		return room;
	}
	
	
	
}
