package ss.week2;

public class Rectangle {
    private int width;
	private int length;
	private int theArea;
	private int thePerimeter;


    //@ private invariant length >= 0;
    //@ private invariant width >= 0;
    //@ private invariant theArea >= 0;
	//@ private invariant thePerimeter >= 0;

  
    /**
     * Create a new <code>Rectangle</code> with the specified length and width.
     */
    /*@
     	requires theLength >= 0 && theWidth >= 0;
     	ensures this.length() == theLength;
     	ensures width() == theWidth;
     */
    public Rectangle(int theLength, int theWidth) {
    	assert theWidth >= 0;
    	assert theLength >= 0;
    	width = theWidth;
    	length = theLength;
    }
    
    /**
     * The length of this Rectangle.
     */
    //@ ensures \result >= 0;
    /*@ pure */ public int length() {
	return length;
    }

    /**
     * The width of this Rectangle.
     */
    //@ ensures \result >= 0;
    /*@ pure */ public int width() {
    	return width;
    }

    public void setLenght(int theLength){
    	length = theLength;
    }
    
    
    public void setWidtht(int theWidth){
    	width = theWidth;
    }
    
    
    
    /**
     * The area of this Rectangle.
     */
    //@ ensures \result >= 0;
    //@ ensures \result == length() * width();
    public int area() {
        assert (length >= 0);
        assert (width >= 0);
        theArea = length * width;
        assert (theArea >= 0);
        assert (theArea == length() * width());
    	return theArea;
    }

    /**
     * The perimeter of this Rectangle.
     */
    //@ ensures \result >= 0;
    //@ ensures \result == 2* (length() + width());
    public int perimeter() {
    	thePerimeter = 2* (length + width);
    	assert (thePerimeter >= 0);
    	assert (thePerimeter >= 2* (length() + width()));
    	return thePerimeter;
    }
}

