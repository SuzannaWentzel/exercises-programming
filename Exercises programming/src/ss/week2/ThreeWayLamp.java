package ss.week2;

public class ThreeWayLamp {
	
	public enum State {
		OFF, 
		LOW, 
		MEDIUM, 
		HIGH;
	}
	
	private State setting;
	
	//@ ensures getSetting() == State.OFF;
	public ThreeWayLamp() {
		setting = State.OFF;
	}
	
	//@ ensures \result == State.OFF || \result == State.LOW || \result == State.MEDIUM || \result == State.HIGH;
	/*@ pure */ public State getSetting() {
		assert  setting == State.OFF || setting == State.LOW || setting == State.MEDIUM || setting == State.HIGH;
		return setting;
	}
	
	/*@ ensures \old(getSetting()) == State.OFF ==> getSetting() == State.LOW 
			&& \old(getSetting() == State.LOW) ==> getSetting() == State.MEDIUM 
			&& \old(getSetting()) == State.MEDIUM ==> getSetting() == State.HIGH 
			&& \old(getSetting()) == State.HIGH ==> getSetting() == State.OFF;
	*/
	
	public void switchSetting() {
		assert setting == State.OFF || setting == State.LOW || setting == State.MEDIUM || setting == State.HIGH;
		switch (getSetting()){
			case OFF:
				setting = State.LOW;
				break;
			case LOW:
				setting = State.MEDIUM;
				break;
			case MEDIUM:
				setting = State.HIGH;
				break;
			case HIGH:
				setting = State.OFF;
				break;
			default: break;
		}
	}
}

