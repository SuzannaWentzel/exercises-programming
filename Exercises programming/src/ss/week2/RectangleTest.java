package ss.week2;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;


import ss.week2.Rectangle;

public class RectangleTest {
	    /** Testvariabele for a <tt>Rectangle</tt>-object. */
	    private Rectangle rectangle1;
	    /** Testvariabele for a <tt>Rectangle</tt>-object. */
	    private Rectangle rectangle2;

	    @Before
	    public void setUp() {
	        // initialisation of Rectangle-variables
	        rectangle1 = new Rectangle(1,4);
	        rectangle2 = new Rectangle(5,3);
	    }

	    /**
	     * Test if the initial condition complies to the specification.
	     */
	    @Test
	    public void testInitialcondition() {
	        assertEquals(4, rectangle1.width());
	        assertEquals(1, rectangle1.length());
	        assertEquals(3, rectangle2.width());
	        assertEquals(5, rectangle2.length());
	    }

	    /**
	     * Tests checking in a new guest in an empty room.
	     * Calls <tt>ot.checkin(k101)</tt>.
	     */
	    @Test
	    public void testCheckinEmpty() {
	        assertEquals(4, rectangle1.area());
	        assertEquals(15, rectangle2.area());
	        assertEquals(10, rectangle1.perimeter());
	        assertEquals(16, rectangle2.perimeter());
	    }
}