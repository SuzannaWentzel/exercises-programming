package ss.week2.test;

import ss.week2.hotel.Safe;
import org.junit.Before;
import org.junit.Test;
import ss.week1.Password;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * 
 * @author Suzanna Wentzel
 *
 */

public class SafeTest {

	Safe testSafe1, testSafe2;
	
	/**
	 * Sets the instance variable to a well defined initial value.
	 */
	@Before
	public void setUp () {
		testSafe1 = new Safe(101, "wekunnendit");
		testSafe2 = new Safe(102, "ojazekerwel");
		
	}
	
	
	@Test
	public void testActivate(){
		testSafe1.activate("wekunnendit");
		testSafe2.activate("nopenopenope");
		assertTrue(testSafe1.isActive());	
		assertFalse(testSafe2.isActive());
	}
	
	@Test
	public void testDeactivates(){
		testSafe1.deactivates();
		assertFalse(testSafe1.isActive());	
		assertFalse(testSafe2.isActive());
	}
	
	@Test
	public void testOpen(){
		testSafe1.activate("wekunnendit");
		testSafe1.open("wekunnendit");
		testSafe2.open("nopenopenope");
		assertTrue(testSafe1.isOpen());
		assertFalse(testSafe2.isOpen());
	}
	
	@Test
	public void testClose(){
		testSafe1.close();
		testSafe2.deactivates();
		assertFalse(testSafe1.isOpen());
		assertFalse(testSafe2.isOpen());
	}
	
	@Test
	public void testGetPassword(){
		
	}
}
