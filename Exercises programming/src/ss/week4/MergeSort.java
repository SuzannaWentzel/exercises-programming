package ss.week4;

import java.util.*;

public class MergeSort {
	public static <Elem extends Comparable<Elem>> List<Integer> mergesort(List<Integer> list) {
    		
    		List<Integer> left = new ArrayList<Integer>();
    		List<Integer> right = new ArrayList<Integer>();
    		List<Integer> result = new ArrayList<Integer>();
    		int length = list.size();
    		int center = list.size()/2;
    		
    		if (length == 1){
    			return list;
    		} 
    		else if (list.isEmpty()){
    			return list;
    		}
    		else {
    			for (int i = 0; i < center; i++ ){
    				left.add(list.get(i));
    			}
    			for (int i = center; i < list.size(); i++){
    				right.add(list.get(i));
    			}
    		}
    		
    		left = mergesort(left);
    		right = mergesort(right);
    		
    		int j = 0;
    		int k = 0;
    		while (j < left.size() || k < right.size()){
    			while (j < left.size() && k < right.size()){
    				if (left.get(j) < right.get(k)){
    					result.add(left.get(j));
    					j++;
    				}
    				else if (right.get(k)< left.get(j)){
    					result.add(right.get(k));
    					k++;
    				}
    				else if (left.get(j) == right.get(k)){
    					result.add(left.get(j));
    					result.add(right.get(k));
    					j++;
    					k++;
    				}
    			}
    			if (j < left.size()){
    				result.add(left.get(j));
    				j++;
    			}
    			else if (k < right.size()){
    				result.add(right.get(k));
    				k++;
    			}
    		}
    		
    		list = result;
    		return list;
    		
    }
}

