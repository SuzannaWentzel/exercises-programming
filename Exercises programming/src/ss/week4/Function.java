package ss.week4;

public interface Function {
	public double apply(double argument);
	public Function derivative();
	public String toString();
}
