package ss.week4;

import ss.week4.Sum;

public class Product implements Function {
	Function fun1;
	Function fun2;
	
	public Product(Function function1, Function function2) {
		fun1 = function1;
		fun2 = function2;
	}

	@Override
	public double apply(double argument) {
		return fun1.apply(argument) * fun2.apply(argument);
	}

	@Override
	public Function derivative() {
		Function productDerivative1 = new Product(fun1, fun2.derivative());
		Function productDerivative2 = new Product(fun2, fun1.derivative());
		return new Sum(productDerivative1, productDerivative2);
	}
	
	@Override
	public String toString(){
		return fun1.toString() + " * " + fun2.toString();
	}

}
