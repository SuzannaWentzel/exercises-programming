package ss.week4;

import ss.week4.Exponent;

public class Polynomial implements Function, Integrandable {
	double[] arraydoubles;
	
	public Polynomial(double[] arraydoubles) {
		this.arraydoubles = arraydoubles;
	}

	@Override
	public double apply(double argument) {
		int n = arraydoubles.length;
		double appliedPolynomial = 0;

		for (int i = 0; i < arraydoubles.length; i++){
			Function exponent = new Exponent(n);
			Function polynomial = new LinearProduct(exponent, arraydoubles[n-1]);
			appliedPolynomial = appliedPolynomial + polynomial.apply(argument);
			n --;
		}
		
		return appliedPolynomial;
	}

	@Override
	public Function derivative() {
		Function linPrDer;
		Function prodDer;
		Function derivative = new Constant(0);
		int n = arraydoubles.length;
		
		for (int i = 0; i < arraydoubles.length; i++){
			linPrDer = new LinearProduct(new Exponent(n-1), arraydoubles[n-1]);
			prodDer = new LinearProduct(linPrDer, n);
			derivative = new Sum(derivative, prodDer);
			n--;		
		}
		
		return derivative;
	}
	
	@Override
	public Function integrand(){
		Function linPrInt;
		Function prodInt;
		Function integrand = new Constant(0);
		int n = arraydoubles.length-1;
		
		for (int i = 1; i < arraydoubles.length; i++){
			linPrInt = new LinearProduct(new Exponent(n+1), arraydoubles[n]);
			prodInt = new LinearProduct(linPrInt, n);
			integrand = new Sum(integrand, prodInt);
			n--;
		}
		
		return integrand;
	}

}
