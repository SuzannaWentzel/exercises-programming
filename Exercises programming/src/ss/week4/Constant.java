package ss.week4;

import ss.week4.Function;
import ss.week4.Integrandable;

public class Constant implements Function, Integrandable {
	private double value;
	
	public Constant(double i) {
		value = i;
	}

	@Override
	public double apply(double argument) {
		return value;
	}

	@Override
	public Function derivative() {
		return new Constant(0);
	}

	@Override
	public String toString(){
		return "" + value;
	}
	
	@Override
	public Function integrand(){
		Function inCon = new Product(new Constant(value), new Exponent(1));
		return inCon;
	}
}
