package ss.week4;

import ss.week4.LinearProduct;

public class Exponent implements Function,Integrandable {
	int n;
	Function fun1;
	

	public Exponent(int n) {
		this.n = n;
	}

	@Override
	public double apply(double argument) {
		if (n != 0){
			double appliedExponent = argument;
			for (int i = 1; i <= n-1; i++){
				appliedExponent = appliedExponent*argument;
			}
			return appliedExponent;
		}
		else {
			return 1.0;
		}
	}

	@Override
	public Function derivative() {
		return new LinearProduct(new Exponent (n-1), n);
	}
	
	@Override
	public String toString(){
		return "x^ " + n;
	}
	
	@Override
	public Function integrand(){
		return new LinearProduct(new Exponent(n+1), 1.0/(n+1));
	}

}
