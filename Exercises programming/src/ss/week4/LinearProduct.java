package ss.week4;

public class LinearProduct implements Function, Integrandable {
	Function fun1;
	Function fun2;
	double constant;
	
	public LinearProduct(Function function, double constant) {
		fun1 = function;
		fun2 = new Constant(constant);
		this.constant = constant;
	}

	@Override
	public double apply(double argument) {
		return fun1.apply(argument) * fun2.apply(argument);
	}

	@Override
	public Function derivative() {
		return new LinearProduct(fun1.derivative(), constant);
	}
	
	@Override
	public String toString(){
		return constant + " * " + "(" + fun1 + ")";
	}
	
	@Override
	public Function integrand(){
		if (fun1 instanceof Integrandable){
			Integrandable int1 = (Integrandable) fun1;
			return new LinearProduct(int1.integrand(), constant);
		} else return null;
	}
}
