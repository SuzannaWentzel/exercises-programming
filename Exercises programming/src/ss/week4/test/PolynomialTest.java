package ss.week4.test;

import org.junit.Before;
import org.junit.Test;
import ss.week4.*;
import static org.junit.Assert.assertEquals;

public class PolynomialTest {
	Polynomial polynomial;
    private static final double DELTA = 1e-15;

	

	@Before
	public void stetUp(){
		double[] arraydoubles = {1.0, 2.0, 3.0, 4.0};
		polynomial = new Polynomial(arraydoubles);
	}
	
	
	@Test
	public void testApply() {
		assertEquals(426, polynomial.apply(3), DELTA);
		assertEquals(0, polynomial.apply(0), DELTA);
		assertEquals(10, polynomial.apply(1), DELTA);
	}
	
	@Test
	public void testDerivative() {
		assertEquals(526, polynomial.derivative().apply(3), DELTA);
		assertEquals(1, polynomial.derivative().apply(0), DELTA);
		assertEquals(30, polynomial.derivative().apply(1), DELTA);
		
	}

}
