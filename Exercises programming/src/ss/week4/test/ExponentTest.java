package ss.week4.test;

import org.junit.Test;
import org.junit.Before;
import ss.week4.Exponent;
import ss.week4.LinearProduct;
import ss.week4.Integrandable;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class ExponentTest {

    public static final int CONSTANT_VALUE = 35;
    private static final double DELTA = 1e-15;

    
    @Test
    public void testApply() {
        assertEquals(1, new Exponent(0).apply(CONSTANT_VALUE), DELTA);
        assertEquals(35, new Exponent(1).apply(CONSTANT_VALUE), DELTA);
        assertEquals(1225, new Exponent(2).apply(CONSTANT_VALUE), DELTA);
    }

    @Test
    public void testDerivative() {
        Exponent exponent = new Exponent(3);
        assertTrue(exponent.derivative() instanceof LinearProduct);
        assertEquals(12, exponent.derivative().apply(2), DELTA);
   }
   
   @Test
   public void testIntegrand(){
	   Exponent exponent = new Exponent(3);
	   assertTrue(exponent.integrand() instanceof Integrandable);
	   System.out.println(exponent.integrand().apply(3));
	   assertEquals(81.0/4, exponent.integrand().apply(3), DELTA);
	   assertEquals(1.0/4, exponent.integrand().apply(1), DELTA);
	   assertEquals(0, exponent.integrand().apply(0), DELTA);
   }
    
}
