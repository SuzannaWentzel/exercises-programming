package ss.week4;

public class Sum implements Function, Integrandable {
	Function fun1;
	Function fun2;
	
	public Sum(Function function1, Function function2 ) {
		this.fun1 = function1;
		this.fun2 = function2;
		
	}

	@Override
	public double apply(double argument) {
		return fun1.apply(argument) + fun2.apply(argument);
		
	}

	@Override
	public Function derivative() {
		return new Sum(fun1.derivative(), fun2.derivative());
	}
	
	@Override
	public String toString(){
		return fun1.toString() + " + " + fun2.toString();
	}
	
	@Override
	public Function integrand(){
		if (fun1 instanceof Integrandable && fun2 instanceof Integrandable){
			Integrandable int1 = (Integrandable) fun1;
			Integrandable int2 = (Integrandable) fun2;
			return new Sum(int1.integrand(), int2.integrand());
		} else return null;
	}

}
