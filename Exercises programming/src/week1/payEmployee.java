package week1;

public class payEmployee {
	public static final int hours = 60;
	public static final int rate = 10;
	public static final int normalWeek = 40;
	public static final int rest = hours - normalWeek;
	
	public static void main(String[] args){
		if (hours - normalWeek > 0){
			System.out.println(rest*rate*1.5 + normalWeek*rate);
		} else {
			System.out.println(rate*normalWeek);
		}
	}
}
