package week1;

public class DollarCounter {
	public static double savings = 65.45;
	public static int cents = (int) savings%1 *100;
	public static int dollars = (int) savings;

	public static void main(String[] args){
		System.out.println("dollars: " + dollars + ", cents: " + cents);
	}
}
