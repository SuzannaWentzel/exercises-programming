package week1;

public class ThreeWayLamp {
	
	public static final int OFF = 0;
	public static final int LOW = 1;
	public static final int MEDIUM = 2;
	public static final int HIGH = 3;
	
	public int setting;
	
	
	public ThreeWayLamp() {
		setting = OFF;
	}
	
	public int getSetting() {
		return setting;
	}
	
		public void switchSetting() {
		setting++;
		if (setting % (HIGH + 1) == 0) {
			setting = OFF;
		}
	}
}

