package week1;

public class Sufficient {
	public static final int SUFSCORE = 70;
	public static boolean passed(double score){
		return score >= SUFSCORE;
	}
	
	public static void main(String[] args){
		System.out.println(passed(4.3));
		System.out.println(passed(5.5));
		System.out.println(passed(8.3));
		System.out.println(passed(5.4));


	}
}
